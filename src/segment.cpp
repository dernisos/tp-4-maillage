#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/squared_distance_2.h>
#include <CGAL/Polyhedron_3.h>
#include <iostream>
#include <string> 
#include <fstream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <queue>
#include <cmath>
#include <map>
#include <ctime>


typedef CGAL::Simple_cartesian<double>               		Kernel;
typedef Kernel::Point_3                              		Point_3;
typedef CGAL::Polyhedron_3<Kernel>                   		Polyhedron;
typedef Polyhedron::Vertex_iterator                   		Vertex_iterator;
typedef Polyhedron::Facet_iterator                   		Facet_iterator;
typedef std::map<Polyhedron::Vertex_handle, Point_3> 		CoordMap;
typedef std::map<Polyhedron::Facet_handle, int> 			Facet_int_map;
typedef std::map<Polyhedron::Facet_handle, double>      	Facet_double_map;
typedef std::map<Polyhedron::Facet_handle, bool>      		Facet_bool_map;
typedef std::string 										String;
typedef std::vector<double> 								VectorD;
typedef Polyhedron::Halfedge_around_vertex_circulator 		VertexCirc;
typedef Polyhedron::Halfedge_around_facet_circulator 		Halfedge_facet_circulator;
typedef std::queue<Polyhedron::Facet_handle> 				Facet_queue;
typedef std::vector<int>									Vector_int;
typedef std::vector<double>									Vector_double;
typedef std::vector<Vector_double>							Vector_Color;

class Color{
	char R;
	char G;
	char B;
public:
	Color():R(0.0f), G(0.0f), B(0.0f){}
	Color(char r, char g, char b): R(r), G(g), B(b){}
	char getR() const{ return R; }
	char getG() const{ return G; }
	char getB() const{ return B; }
	void setR(char r){
		R = r;
	}
	void setG(char g){
		G = g;
	}
	void setB(char b){
		B = b;
	}
};


double maxMes = 0.0;
double minMes = DBL_MAX;
double moyMes = 0.0;

double maxAire = 0.0;
double minAire = DBL_MAX;

Facet_double_map seuillageSimple(Polyhedron & P, double * moy){
	double perimetre = 0.0;
	double som = 0.0;
	Facet_double_map  perimMap;
	for (Facet_iterator i = P.facets_begin(); i != P.facets_end(); ++i) {
		perimetre = 0.0;
		Halfedge_facet_circulator j = i->facet_begin();
		CGAL_assertion( CGAL::circulator_size(j) >= 3);
		
		do {
			perimetre += sqrt(CGAL::squared_distance(j->vertex()->point(), j->opposite()->vertex()->point()));
			           
		} while ( ++j != i->facet_begin());
		som += perimetre;
		perimMap[i] = perimetre;
	}
	* moy = som/perimMap.size();
	return perimMap;
}

Facet_int_map colorMap(Facet_double_map  & perimMap, double moyenne){
	Facet_int_map  idMap;
	Facet_double_map ::iterator itPM;
	for(itPM = perimMap.begin(); itPM != perimMap.end(); itPM++){
		if(itPM->second < moyenne){
			idMap[itPM->first] = 0;
		}
		else{
			idMap[itPM->first] = 1;
		}
	}
	return idMap;
}

void calculPerimetre(Polyhedron & mesh, Facet_double_map & dm){
	int nbFacets = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		Halfedge_facet_circulator j = i->facet_begin();
		CGAL_assertion(CGAL::circulator_size(j) >= 3);
		double perim = 0.0;
		do{
			perim = perim + sqrt(CGAL::squared_distance(j->vertex()->point(), j->opposite()->vertex()->point()));
			j++;
		}while(j != i->facet_begin());
		if(perim > maxMes)
			maxMes = perim;
		if(perim < minMes)
			minMes = perim;
		dm[i] = perim;
		moyMes = moyMes + perim;
		nbFacets++;
	}
	moyMes = moyMes / (double) nbFacets;
}

void calculAire(Polyhedron &mesh, Facet_double_map &map) {
	Facet_iterator i = mesh.facets_begin();
	Vector_double dist;
	double point, aire;
	while (i != mesh.facets_end()) {
		Halfedge_facet_circulator j = i->facet_begin();
		CGAL_assertion(CGAL::circulator_size(j) >= 3);
		do {
			dist.push_back(sqrt(CGAL::squared_distance(j->vertex()->point(), j->opposite()->vertex()->point())));
			++j;
		} while (j != i->facet_begin());
		point = (dist[0] + dist[1] + dist[2])/2;
		aire = sqrt(point * (point - dist[0]) * (point - dist[1]) * (point - dist[2]));
		map[i] = aire;
		if (aire > maxAire) maxAire = aire;
		if (aire < minAire) minAire = aire;
		++i;
	}
}

Facet_int_map seuillageOtsu(Polyhedron &mesh, Facet_double_map &valeurs){
	Facet_int_map result;
	Vector_int histo = Vector_int(64);
	int nbFacets = valeurs.size();
	int colonhisto, threshold = 0;
	int sum = 0, sumB = 0, q1 = 0, q2;
	double u1, u2, cu, max = 0.0;
	Facet_double_map aire;
	calculAire(mesh,aire);
	for(Vector_int::iterator it = histo.begin(); it != histo.end(); it++){
		*it = 0;
	}
	Facet_iterator i = mesh.facets_begin();
	while (i != mesh.facets_end()){
		colonhisto = ((valeurs[i] - minMes) / (maxMes - minMes)) * 64;
		histo[colonhisto] += ((aire[i] - minAire) / (maxAire - minAire)) *100;
		++i;
	}
	for(int k =0; k<63; k++){
		sum += k * histo[k];
	}
	for(int k = 0; k<63; ++k){
		q1 += histo[k];
		if(q1 == 0) continue;
		q2 = nbFacets - q1;
		sumB += k * histo[k];
		u1 = (double)sumB / (double)q1;
		u2 = (double)(sum - sumB) / (double)q2;
		cu = (double)q1 * (double)q2 * (u1 - u2) * (u1 - u2);
		if(cu>max){
			threshold = k;
			max = cu;
		}
	}
	for(Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end();i++){
		colonhisto = (valeurs[i] - minMes)/(maxMes-minMes) *64;
		if(colonhisto < threshold){
			result[i] = 0;
		}
		else{
			result[i] = 1;
		}
	}
	return result;
}


int segmentationParCC(Polyhedron & mesh, Facet_int_map & segmentation, Facet_int_map &result){
	Polyhedron::Facet_handle fhand;
	int nbClass = 0;
	int segcour;
	Facet_queue qu;
	Facet_bool_map parc;
	for(Facet_iterator i = mesh.facets_begin(); i!= mesh.facets_end();i++){
		parc[i] = false;
	} 
	for(Facet_iterator i = mesh.facets_begin(); i!= mesh.facets_end();i++){
		if(!parc[i]){
			segcour = segmentation[i]; //On recupère la classe de la facet
			qu.push(i); //La facet est ajoutée dans la queue
			parc[i] = true; //La facet a été vue
			while(!qu.empty()){
				fhand = qu.front();
				qu.pop();
				result[fhand] = nbClass;
				Halfedge_facet_circulator j = fhand->facet_begin();
				do{
					Polyhedron::Facet_handle f = j->opposite()->facet();
					if(parc[f] == false && segmentation[f] == segcour){ //On ajoute la facet à la queue si elle n'a pas déjà été vue et si elle appartient à la même classe que la facet précédente
						qu.push(f);
						parc[f] = true;
					}
					++j;
				}while(j != fhand->facet_begin());
			}
			nbClass++; //On change de classe pour colorier les autres faces
		}
	} 
	std::cout << "Nombre de classe : " << nbClass << std::endl;
	
	return nbClass;
}

void sauverSegmentation(Facet_int_map  & idMap, String filename, Polyhedron & P, int nbClass){
	std::ofstream fichier(filename.c_str(), std::ofstream::out);
	CGAL::set_ascii_mode( std::cout);
	Facet_int_map ::iterator it = idMap.begin();
	Vector_Color VectColor;
	if(nbClass == 0){
		nbClass = 2;
	}
	for(int i = 0; i<nbClass; i++){
		Vector_double c;
		for(int j = 0; j<3;j++){
			c.push_back((double) (std::rand() % 256));
		}
		VectColor.push_back(c);
	}
	
	if(fichier){
		fichier << "OFF" << std::endl << P.size_of_vertices() << ' ' << P.size_of_facets() << " 0" << std::endl; 
		std::copy( P.points_begin(), P.points_end(), std::ostream_iterator<Point_3>( fichier, "\n")); 
		
		while(it != idMap.end()){
			Halfedge_facet_circulator  j = it->first->facet_begin();
			fichier << CGAL::circulator_size(j) << ' ';
			do {
				fichier << ' ' << std::distance(P.vertices_begin(), j->vertex());
			} while ( ++j != it->first->facet_begin());
			
			fichier << ' ' << VectColor[it->second][0] << ' ' << VectColor[it->second][1] << ' ' << VectColor[it->second][2]; 
			fichier << std::endl;
			it++;
			
		}
		fichier.close();
	}
	else{
		std::cout << "Impossible d'ouvrir le fichier !" << std::endl;
	}
}

int main(int argc, char* argv[]){
	String filename;
	Facet_double_map  perimMap;
	double moyenne = 0.0;
	Facet_int_map  idMap, resMap;
	int nbClass = 0;
	if (argc < 2) {
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}
	if(argc < 3){
		std::cout << "Le fichier OFF créé aura le nom par défaut 'TestDefaut.off'." << std::endl;
		filename = "TestDefaut.off";
	}
	else{
		filename = argv[2];
	}
	Polyhedron mesh;
	std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty()) {
		std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
		return 1;
	}
	
	std::srand(std::time(0));
	
	//perimMap = seuillageSimple(mesh, &moyenne);
	//idMap = colorMap(perimMap, moyenne);
	calculPerimetre(mesh,perimMap);
	idMap = seuillageOtsu(mesh,perimMap);
	nbClass = segmentationParCC(mesh,idMap, resMap);
	sauverSegmentation(resMap, filename, mesh, nbClass);	
	
	return 0;
}
