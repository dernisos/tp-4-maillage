#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
#include <string>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/squared_distance_3.h> //for 3D functions

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef Kernel::Point_3                              Point_3;
typedef CGAL::Polyhedron_3<Kernel>                   Polyhedron;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfegde_facet_circulator;



void OffWrite(Polyhedron& P, std::map<Polyhedron::Facet_handle, double> map){
	std::fstream myfile;
	myfile.open("../Rendu.off",std::fstream::out);
	myfile << "OFF" << std::endl << P.size_of_vertices() << ' '
              << P.size_of_facets() << " 0" << std::endl;
    std::copy( P.points_begin(), P.points_end(),std::ostream_iterator<Point_3>( myfile, "\n"));
    int s = 0;
    for(std::map<Polyhedron::Facet_handle, double>::iterator it = map.begin();it != map.end(); ++it){
		Halfegde_facet_circulator j = it->first->facet_begin();
		myfile << CGAL::circulator_size(j) << ' ';
		do {
            myfile << ' ' << std::distance(P.vertices_begin(), j->vertex());
        } while ( ++j != it->first->facet_begin());
        if(s == 0){
				myfile << "  255 0 0";
				s= 1;
			}
			else{
				myfile << "  0 0 255";
				s=0;
			}
        myfile << std::endl;
	}
    myfile.close();
           
}


int main(int argc, char* argv[])
{
	std::map<Polyhedron::Facet_handle, double> mapping;
	
	if (argc < 2) {
		std::cerr << "Il manque un paramÃ¨tre au programme. Veuillez lui donner en entrÃ©e un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
  std::ifstream input(argv[1]);
	 if (!input || !(input >> mesh) || mesh.is_empty()) {
    std::cerr << "Le fichier donnÃ© n'est pas un fichier off valide." << std::endl;
    return 1;
  }
  
  unsigned int nbVerts = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		++nbVerts;
	}
	std::cout << "Nombre de sommets: " << nbVerts << std::endl;
	
	unsigned int nbEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i) {
		++nbEdges;
	}
	nbEdges /= 2;
	std::cout << "Nombre d'arÃªtes: " << nbEdges << std::endl;

	unsigned int nbFaces = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		++nbFaces;
	}
	std::cout << "Nombre de faces: " << nbFaces << std::endl;
	
	unsigned int euler = nbVerts - nbEdges + nbFaces;
	unsigned int genus = (2 - euler) / 2;
	std::cout << "En supposant que le maillage contienne une unique surface sans bord, alors son genre est de " << genus << std::endl;
  
	
	for (Facet_iterator i = mesh.facets_begin();i != mesh.facets_end();++i){
		double perimetre = 0;
		Halfegde_facet_circulator j = i->facet_begin();
		CGAL_assertion( CGAL::circulator_size(j) >= 3);
		std::cout << "Face à " << CGAL::circulator_size(j) << " cote";
		do{
			perimetre += squared_distance(j->vertex()->point(),j->opposite()->vertex()->point());
		}while(++j != i->facet_begin());
		std::cout << " avec perimetre de la face de:" << perimetre << std::endl;
		mapping[i] = perimetre;
	}
	OffWrite(mesh,mapping);
  
	return 0;
}
