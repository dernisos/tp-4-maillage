#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/squared_distance_3.h> //for 3D functions

typedef CGAL::Exact_predicates_inexact_constructions_kernel 	Kernel;
typedef Kernel::Point_3                              			Point_3;
typedef CGAL::Polyhedron_3<Kernel>                   			Polyhedron;
typedef Polyhedron::Facet_iterator 								Facet_iterator;
typedef Polyhedron::Vertex_handle	 							Vertex_handle;
typedef Polyhedron::Vertex_iterator 							Vertex_iterator;
typedef Polyhedron::Halfedge_iterator 							Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator 			Halfegde_facet_circulator;
typedef std::map<Polyhedron::Vertex_handle,Point_3> 			Vertex_coord_map;
typedef Polyhedron::Halfedge_around_vertex_circulator 			Halfedge_vertex_circulator;
typedef std::map<Polyhedron::Vertex_handle, double>      		Vertex_double_map;
typedef std::map<Polyhedron::Vertex_handle, bool>      			Vertex_bool_map;
typedef std::map<Polyhedron::Vertex_handle, int>      			Vertex_int_map;



Vertex_coord_map getCoords(Polyhedron & mesh){
	Vertex_coord_map Vm;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		Vm[i] = i->point();
	}	
	return Vm;
}

Vertex_coord_map laplacianSmoothing(Polyhedron & mesh, Vertex_coord_map & coords){
	Vertex_coord_map cordtemp;
	
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		float xtemp = 0;
		float ytemp = 0;
		float ztemp = 0;
		int nbV = 0;
		Halfedge_vertex_circulator j(i->vertex_begin()), e_end(j);
		do{
			xtemp += coords[j->opposite()->vertex()].x();
			ytemp += coords[j->opposite()->vertex()].y();
			ztemp += coords[j->opposite()->vertex()].z();
			++nbV;
			++j;
		}while(j != e_end);
		cordtemp[i] = Point_3(xtemp/nbV,ytemp/nbV,ztemp/nbV);
		//std::cout << "coord 1 : " << cordtemp[i] << " coord 2: " << coords[i] << std::endl;
	}
	return cordtemp;
}

double rayonMoy(Polyhedron & mesh){
	double res = 0;
	int i = 0;
	for(Halfedge_iterator it = mesh.halfedges_begin(); it != mesh.halfedges_end();++it){
		res += squared_distance(it->vertex()->point(),it->opposite()->vertex()->point());
		++i;
	}
	res = res/i;
	return res;
}



Vertex_coord_map laplacianSmoothingRadius(Polyhedron & mesh, Vertex_coord_map &coords, double radius) {

	Vertex_coord_map res;
	Vertex_int_map flag;
	std::queue<Vertex_handle> queue;
	for (Vertex_iterator it = mesh.vertices_begin(); it!= mesh.vertices_end(); ++it) {
		double nbV = 0; 
		double sumX =0;
		double sumY =0;
		double sumZ =0;
		for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) flag[i] = 0;
		while(!queue.empty()) queue.pop();

		flag[it] = 1;
		queue.push(it);
		while(!queue.empty()) {
			Vertex_handle j = queue.front();
			
			double rad = sqrt(CGAL::squared_distance(it->point(), j->point()))/radius;
			

			//Linéaire
			//double impact = 1-rad;
			//Carré convexe
			//double impact = (1-rad)*(1-rad);
			//Carré concave
			double impact = 1-(1-rad)*(1-rad);


			sumX += j->point().x()*impact;
			sumY += j->point().y()*impact;
			sumZ += j->point().z()*impact;
			nbV+=impact;

			queue.pop();
		
			Halfedge_vertex_circulator c = j->vertex_begin();
			do {
				
				Vertex_handle it2 = c->opposite()->vertex();
				if (flag[it2]==0 && sqrt(CGAL::squared_distance(it2->point(), it->point())) < radius ) {
					queue.push(it2);
					flag[it2] = 1;
				}
			} while (++c != j->vertex_begin());
		}	
		res[it] = Point_3(sumX/nbV, sumY/nbV, sumZ/nbV); 
	}
	return res;
}

void save(Polyhedron & mesh, Vertex_coord_map & coords){
	std::fstream myfile;
	myfile.open("../lissage.off",std::fstream::out);
	myfile << "OFF" << std::endl << mesh.size_of_vertices() << ' '
              << mesh.size_of_facets() << " 0" << std::endl;
    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		myfile << coords[i] << std::endl;
	}          
    int s = 0;
    for(Facet_iterator it = mesh.facets_begin();it != mesh.facets_end(); ++it){
		Halfegde_facet_circulator j = it->facet_begin();
		myfile << CGAL::circulator_size(j) << ' ';
		do {
            myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
        } while ( ++j != it->facet_begin());
        myfile << std::endl;
	}
    myfile.close();
}

void saveColorDistance(Polyhedron & mesh, Vertex_coord_map & coords1, Vertex_coord_map & coords2){
	std::fstream myfile;	
	double max = 0;
	double min = DBL_MAX;
	Vertex_double_map dist;
	myfile.open("../lissage_color.off",std::fstream::out);
	myfile << "COFF" << std::endl << mesh.size_of_vertices() << ' '
              << mesh.size_of_facets() << " 0" << std::endl;
    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
			dist[i] = (sqrt(CGAL::squared_distance(coords1[i], coords2[i])));
			if(dist[i]>max) max = dist[i];
			if(dist[i]<min) min = dist[i];
	}     

    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		myfile << coords1[i] << " 160 40 " << ((dist[i]-min)*(255/(max-min))) << " 1" << std::endl;
		
	}          
    int s = 0;
    for(Facet_iterator it = mesh.facets_begin();it != mesh.facets_end(); ++it){
		Halfegde_facet_circulator j = it->facet_begin();
		myfile << CGAL::circulator_size(j) << ' ';
		do {
            myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
        } while ( ++j != it->facet_begin());
        myfile << std::endl;
	}
    myfile.close();
} 

int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cerr << "Il manque un paramÃ¨tre au programme. Veuillez lui donner en entrÃ©e un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
	Vertex_coord_map mcoord, mcoord2, mcoord3;
	std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty()) {
		std::cerr << "Le fichier donnÃ© n'est pas un fichier off valide." << std::endl;
		return 1;
	}
	rayonMoy(mesh);
	mcoord = getCoords(mesh);
	mcoord2 = laplacianSmoothing(mesh,mcoord);
	mcoord3 = laplacianSmoothingRadius(mesh, mcoord, 2);
	save(mesh,mcoord2);
	saveColorDistance(mesh,mcoord2,mcoord);
	return 0;
}
